#!/bin/bash

# Copyleft v3d/crash-stop@hacklab01/fubar.space/formatc.hr, use at your own peril.
# For newest (buggy) version goto https://gitlab.com/hacklab01/MegaGlitchATron

#pretty colors
black=$(tput setaf 0)
red=$(tput setaf 1)
green=$(tput setaf 2)
yellow=$(tput setaf 3)
blue=$(tput setaf 4)  
magenta=$(tput setaf 5)
cyan=$(tput setaf 6)   
white=$(tput setaf 7)  

# check whitch OS we're using and set the imagemagick command accordingly
# from https://stackoverflow.com/a/8597411

if [[ "$OSTYPE" == "linux-gnu"* ]]; then
  # Linux
  imagick_command=""
elif [[ "$OSTYPE" == "darwin"* ]]; then
  # Mac OSX
  imagick_command=""
elif [[ "$OSTYPE" == "cygwin" ]]; then
  # POSIX compatibility layer and Linux environment emulation for Windows
  imagick_command=""
elif [[ "$OSTYPE" == "msys" ]]; then
  # Lightweight shell and GNU utilities compiled for Windows (part of MinGW)
  imagick_command="magick"
elif [[ "$OSTYPE" == "win32" ]]; then
  # 32 bit windows - untested
  imagick_command="magick"
elif [[ "$OSTYPE" == "freebsd"* ]]; then
  # freeBSD
  imagick_command=""
else
  # Unknown OS
  echo "Unknown OS, go into the script and edit the imagick_command variable manually (can be magick or convert)"
fi

# Make a help function we can reuse anywhere
Help()
{
   # Display Help
   echo "MegaGlitchaTron - A command line glitch art script"
   echo
   echo "Syntax: megaglitchatron.sh [-i|m|o]"
   echo "options:"
   echo "i - Set input file name."
   echo "m - Set mode: hex, son or gif."
   echo "modes:"
   echo "hex: uses sed to search and replace a string. Supported input / output: video or image file."
   echo "son: uses ffmpeg to do sonification (treats video / images as audio) Supported input/output: video or image file."
   echo ""
   echo "gif: creates a glitched gif our of a static image. Supported input: image file, output: GIF. "
   echo "PBM format - black and white output"
   echo "PGM format - greyscale output"
   echo "PAM format - color output"
   echo "o - Set output file name."
}

# Cleanup function

ask_cleanup() {
    while true; do
      read -p "${green}Clean up after (keep only output)? Y(es) / N(o) [Y]: " yn
      case $yn in
        [yY] | [yY][eE][sS] | "" )  # Accepts 'y', 'Y', 'yes', 'YES', or empty input
          clean="true"
          break
          ;;
        [nN] | [nN][oO] )  # Accepts 'n', 'N', 'no', 'NO'
          clean="false"
          break
          ;;
        * )
          echo "${red}Invalid response"
          ;;
      esac
    done
}

#TODO: not working, filenames need to be adjusted to fit all modes

transplant_audio() {
      read -p "${green}Transplant source file audio onto glitched and baked video? (Y/N, default: N): " transplant_audio
    transplant_audio=${transplant_audio:-N}
    if [ "$transplant_audio" == "Y" ] || [ "$transplant_audio" == "y" ]; then
        ffmpeg -v quiet -stats -i "$output" -i "$input" -c copy -map 0:v:0 -map 1:a:0 -shortest "$output_with_sound.mp4" -y
    fi
}

hex_mode() {
    now="$(date +'%Y%m%d-%H%M%S')"
    read -p "${green}Enter hex pattern for replacement (default: s/\xDA/\x0E\xDA/g, 'random' for random hex): " hex

    if [ "$hex" == "random" ]; then
        hex="s/\\x$(openssl rand -hex 1)/\\x$(openssl rand -hex 1)\\x$(openssl rand -hex 1)/g"
    else
        hex="${hex:-s/\xDA/\x0E\xDA/g}"
    fi

    mkdir "$now-$input-hex-frames"
    ffmpeg -v quiet -stats -i "$input" "$now-$input-hex-frames/%05d.bmp"
    echo "Doing hex replacement $hex"
    for i in "$now-$input-hex-frames"/*.bmp; do
        sed "$hex" "$i" > temp
        mv temp "$i"
    done
    ffmpeg -v quiet -stats -framerate 25 -i "$now-$input-hex-frames/%05d.bmp" -pixel_format yuv420p -c:v h264 -q:v 0 "$output" -y
    #not working yet: transplant_audio

    # Clean up
    if [ "$clean" == true ]; then
        echo "Cleaning up..."
        rm "$now-$input-hex-frames"/*
        rmdir "$now-$input-hex-frames"
        rm temp
    else
        exit 0
    fi

    # We're done, so exit cleanly
    exit 0
}

son_mode() {
  # Effect options for sonification
  effect_options=("crusher" "phaser" "chorus" "eesser")
  # Prompt the user to select the effect
  # Check https://www.vacing.com/ffmpeg_audio_filters/index.html 
  # or https://www.ffmpeg.org/ffmpeg-filters.html for extra effects

  echo "${green}Select an effect for sonification:"
  select fx in "${effect_options[@]}"
  do
    case $fx in
      "crusher")
        effect="acrusher=level_in=18:level_out=2:bits=1:mode=log:aa=1,adelay=1500"
        break
        ;;
      "phaser")
        effect="aphaser=type=t:speed=1:decay=0.1"
        break
        ;;
      "chorus")
        effect="chorus=0.5:0.9:50|60|70:0.3|0.22|0.3:0.25|0.4|0.3:2|2.3|1.3"
        break
        ;;
      "eesser")
        effect="deesser=i=1:s=e[a];[a]aeval=val(ch)*10:c=same"
        break
        ;;
      "custom")
        read -p "Enter your custom effect string: " custom_effect
        effect="$custom_effect"
        break
        ;;
      *)
        echo "Invalid option. Please try again."
        ;;
    esac

  done
  # Check video resolution and set filename
  video_size=$(ffprobe -v error -select_streams v:0 -show_entries stream=width,height -of csv=s=x:p=0 "$input")
  output="$output-$video_size-$fx"

  # Convert input video to raw.
  echo "${magenta}Converting video to raw"
  echo "${blue}Command used: ${yellow}ffmpeg -i "$input" -c:v rawvideo -pixel_format yuv420p "$output-raw-prep.yuv" -y${white}"
  ffmpeg -v quiet -stats -i "$input" -c:v rawvideo -pixel_format yuv420p "$output-raw-prep.yuv" -y

  # Apply sonification effect
  echo "${magenta}Applying sonification effect"
  echo "${blue}Command used: ${yellow}ffmpeg -f alaw -i "$output-raw-prep.yuv" -af "$effect" -f alaw "$output-sonified-output.yuv" -y${white}"
  ffmpeg -v quiet -stats -f alaw -i "$output-raw-prep.yuv" -af "$effect" -f alaw "$output-sonified-output.yuv" -y

  # Convert the sonified raw back into video.
  echo "${magenta}Converting the sonified raw back into video"
  echo "${blue}Command used: ${yellow}ffmpeg -v quiet -stats -f rawvideo -framerate 30 -pixel_format yuv420p -video_size "$video_size" -i "$output-sonified-output.yuv" -c:v h264 -q:v 0 "$output-sonified-bake.mp4" -y${white}"
  ffmpeg -v quiet -stats -f rawvideo -framerate 30 -pixel_format yuv420p -video_size "$video_size" -i "$output-sonified-output.yuv" -c:v h264 -q:v 0 "$output-sonified-bake.mp4" -y

  # Todo: transplant source file audio onto glitched and baked video.

  # Remove any yuv files we created (they tend to be huge!)
  if [ "$clean" == true ]; then
    echo "Cleaning up..."
    # Remove everything the script made
    rm *.yuv
  else
    # Or exit
    exit 0
  fi

  exit 0

}

gif_mode() {
  now="$(date +'%Y%m%d-%H%M%S')"
  # Pick a format to use for making the frames
  select pg in "PBM format - black and white output" "PGM format - greyscale output" "PPM format - color output"
  do
    case $pg in 
      "PBM format - black and white output")
        format="pbm"
        break;;
      "PGM format - greyscale output")
        format="pgm"
        break;;
      "PPM format - color output")
        format="ppm"
        break;;
    esac
  done
  echo -n "How many iterations (whole number) : " 
  # Read variable n from user input.
  read n
  # Set variable i to 0 (i will hold the number of iterations until it reaches n)
  i=0
  # 
  # Resize info here https://imagemagick.org/Usage/resize/#resize
  # Make a directory that will hold the frames
  mkdir $now-$input-frames
  # We set $imagick_command earlier, depending on our OS
  $imagick_command convert -resize 640x480\! $input test.$format
  # Run while loop while $i is less then (-lt) $n (run help test in your terminal to see all supported test operators in your shell)
  while [ $i -lt $n ]
    do 
      # Increment $i by 1 for each loop as long as the condition is true
      ((i++))
      # Use sed to transfer the header of resized $input file
  #   sed '2,$d' test.$format > head.$format;
      # Strip header ( to avoid damaging it)
  #   sed '1d'  test.$format > swap.$format
      # Generate random numbers
      # Generate no from 1 to 64
      seed1=$(( $RANDOM % 32 + 1 ))
      seed2=$(( $RANDOM % 64 + 1 ))
      seed3=$(( $RANDOM % 32 + 1 ))
      seed4=$(( $RANDOM % 64 + 1 ))
      # Convert to hex
      from=$(printf '%x\n' $seed1)
      to=$(printf '%x\n' $seed2)
      from2=$(printf '%x\n' $seed3)
      to2=$(printf '%x\n' $seed4)      
      #xxd -p swap.$format | sed 's/'$from''$from2'/'$to''$to2'/g' | xxd -p -r > help.$format
      sed -b '5,$s/'"$from"/"$to""$to2"'/g' test.$format > $now-$input-frames/$i.$format
      # Put the header back
  #   cat head.$format help.$format > headswap.$format;
      # Convert headswap.$format image-$i.ppm 
  #    $imagick_command convert headswap.$format $now-$input-frames/$i.ppm
      # Clean up
  #    rm help.$format
      # Finish our while loop
  done
  # Morphgif
  # Save input
  mv $input saved_input
  # Convert frames to gif
  $imagick_command convert -delay 5 $now-$input-frames/*.$format -morph 14 gifatron-$from-$from2-$to-$to2-$output.gif
  mv saved_input $input
  # Clean up
          # If user chose yes to clean (clean is set to true, mind the quotes!)
  if [ "$clean" == true ]
  then
    echo "Cleaning up..."
    # Remove everything the script made
    rm -rf $now-$input-frames/
    rm *.ppm
    rm *.$format        
  else 
    # Or exit (if $clean is false)
    exit 0
  fi
  exit 0
}

# define variables as null (they need to hold a string while checking, might aswell be "null")
input="null"
mode="null"
output="null"
fx="null"

while getopts i:m:f:o: flag
do
    case "${flag}" in
        i) input=${OPTARG};;
        m) mode=${OPTARG};;
        f) fx=${OPTARG};;
        o) output=${OPTARG};;
    esac
done

# What happens if we type in the wrong options?
if [ $mode != "hex" ] && [ $mode != "son" ] && [ $mode != "gif" ] && [ $output == "null" ] && [ $input = "null" ] 
then
# Display a message, followed by the help function defined eariler
  echo "Invalid / empty / unknown option/s, here's the help:"
# Run the help function
  Help
# Exit cleanly
  exit 0
# else if (define other cases)
elif [ $mode == "hex" ]
then
        # Cleanup or no?
        ask_cleanup
        echo "Cleanup option selected: $clean"
        hex_mode
        # TODO: transplant_audio
        

elif [ $mode == "son" ]
then    
        # Cleanup or no?
        ask_cleanup
        echo "Cleanup option selected: $clean"
        son_mode
 
elif [ $mode == "gif" ]
  then
          # Cleanup or no?
          ask_cleanup
          echo "Cleanup option selected: $clean"
          gif_mode

fi
exit 0
