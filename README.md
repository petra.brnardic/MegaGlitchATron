```
╔╦╗┌─┐┌─┐┌─┐╔═╗┬  ┬┌┬┐┌─┐┬ ┬┌─┐╔╦╗┬─┐┌─┐┌┐┌
║║║├┤ │ ┬├─┤║ ╦│  │ │ │  ├─┤├─┤ ║ ├┬┘│ ││││
╩ ╩└─┘└─┘┴ ┴╚═╝┴─┘┴ ┴ └─┘┴ ┴┴ ┴ ╩ ┴└─└─┘┘└┘                                                      
```
![megaglitchatron](https://v3d.space/img/megaglitchatron.gif)

MegaGlitchaTron - A Command Line Glitch Art Script

Software needed:

- Git Bash for Windows https://git-scm.com/downloads (includes the bash shell and software such as sed – a stream editor)
- MacOS ships with everything installed
- Linux should use bash shell and install sed (included in most distros)
- FFmpeg is used for preparing input material, stabilizing output, video / image edits
- FFmpeg install instructions for MacOS and Windows: https://formatc.hr/datamosh101/
- to use FFmpeg on Linux install it via package manager

When everything is installed:
- grab code (clone or just download): https://gitlab.com/hacklab01/MegaGlitchATron/
- put your files (video or image) in the same directory where you put the script and run it. 

Example: 

Take a file called input.mp4, apply a sonification effect to it and save output to a file called output.mp4 (actual output file name may differ).

```
bash mgt.sh -i input.mp4 -m son -o output.mp4
```

```
Syntax: mgt.sh [-i|m|o]

Options:
i - Set input file name.
m - Set mode: hex, son or gif.
o - Set output file name.

Modes:
hex: uses sed to search and replace a string.
son: uses ffmpeg to do sonification (treats video / images as audio).
gif: creates a glitched gif out of a static image.
PBM format - black and white output
PGM format - greyscale output
PAM format - color output

```
